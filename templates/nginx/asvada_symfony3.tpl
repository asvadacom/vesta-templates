server {
    listen      %ip%:%proxy_port%;
    server_name %domain_idn% %alias_idn%;


	error_log  %home%/%user%/web/%domain_idn%/var/logs/error.nginx.no_ssl.log error;

    location / {
        proxy_pass      http://%ip%:%web_port%;
        location ~* ^.+\.(%proxy_extentions%)$ {
            root           %home%/%user%/web/%domain_idn%/web;
            access_log     %home%/%user%/web/%domain_idn%/var/logs/access.nginx.no_ssl.log combined;
            access_log     %home%/%user%/web/%domain_idn%/var/logs/access.nginx.no_ssl.bytes.log bytes;
            expires        max;
            try_files      $uri @fallback;
        }
    }

    location @fallback {
        proxy_pass      http://%ip%:%web_port%;
    }

    location ~ /\.ht    {return 404;}
    location ~ /\.svn/  {return 404;}
    location ~ /\.git/  {return 404;}
    location ~ /\.hg/   {return 404;}
    location ~ /\.bzr/  {return 404;}

    #required for letsencrypt
    include %home%/%user%/conf/web/nginx.%domain_idn%.conf_letsencrypt;
}


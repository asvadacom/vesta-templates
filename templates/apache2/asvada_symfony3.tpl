<VirtualHost %ip%:%web_port%>
	#Define is_no_ssl
	Define host_name %domain_idn%
	Define protocol_prefix "no_ssl"

	ServerName %domain_idn%
	%alias_string%
	ServerAdmin %email%
	DocumentRoot %home%/%user%/web/%domain_idn%/web
#   %docroot%
	Alias /vstats/ %home%/%user%/web/%domain_idn%/stats/
#	SuexecUserGroup %user% %group%
#    %web_system%

	CustomLog %home%/%user%/web/%domain_idn%/var/logs/access.apache.${protocol_prefix}.bytes.log bytes
	CustomLog %home%/%user%/web/%domain_idn%/var/logs/access.apache.${protocol_prefix}.log combined
	ErrorLog %home%/%user%/web/%domain_idn%/var/logs/error.apache.${protocol_prefix}.log
	<Directory %home%/%user%/web/%domain_idn%/web>
		AllowOverride All
		Options +Includes -Indexes +ExecCGI
		<IfDefine is_ssl>
			SSLRequireSSL
		</IfDefine>
#       php_admin_value open_basedir %home%/%user%/web/%domain_idn%:%home%/%user%/tmp
		php_admin_value upload_tmp_dir %home%/%user%/tmp
#       php_admin_value session.save_path %home%/%user%/tmp
	</Directory>
	<Directory %home%/%user%/web/%domain_idn%/stats>
		AllowOverride All
	</Directory>
	<IfDefine is_ssl>
		SSLEngine on
		SSLVerifyClient none
		SSLCertificateFile %home%/%user%/conf/web/ssl.%domain_idn%.crt
		SSLCertificateKeyFile %home%/%user%/conf/web/ssl.%domain_idn%.key
		SSLCertificateChainFile %home%/%user%/conf/web/ssl.%domain_idn%.ca
	</IfDefine>

	<IfModule mod_ruid2.c>
		RMode config
		RUidGid %user% %group%
		RGroups www-data
	</IfModule>
	<IfModule itk.c>
		AssignUserID %user% %group%
	</IfModule>

</VirtualHost>
